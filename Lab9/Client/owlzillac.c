//
// Created by tommydrum on 5/23/16.
//

#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <stdlib.h>
#include <getopt.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <sys/stat.h>
#include <fcntl.h>

#define EOT_CHAR '\04'
#define OPTIONS     "p:i:hvV"

void PromptForCommand(int socketfd);

bool ProcessCommand(char *cmd, int socketfd);

int ConnectToServer(struct in_addr ip, int port);

bool verbose = false;
double version = 1.0;

int main(int argc, char **argv) {
    //var declarations
    char verboseArgumentsReceived[6] = {'\0'};
    int port = 0;
    int socketfd;
    char ipTemp[16] = {'\0'};
    struct in_addr ip;
    //code
    int c;
    while ((c = getopt(argc, argv, OPTIONS)) != -1) //p:i:hvV
    {
        switch (c) {
            case 'p':
                port = atoi(optarg);
                strcat(verboseArgumentsReceived, "p");
                break;
            case 'i':
                strcpy(ipTemp, optarg);
                if (inet_pton(AF_INET, ipTemp, &ip) != 1)
                    fprintf(stderr, "inet_pton failed!\n");
                else
                    fprintf(stderr, "inet-pton completed sucessfully\n");
                strcat(verboseArgumentsReceived, "i");
                break;
            case 'h':
                printf("OwlZilla Client Help: \n");
                printf("\t-v\tVerbose mode\n");
                printf("\t-p #####\tSpecify Port\n");
                printf("\t-i ip_addr\tSpecify IP Address\n");
                printf("\t-V\tVersion\n");
                printf("\t-h\tThis help message\n");
                exit(EXIT_SUCCESS);
            case 'v':
                verbose = true;
                strcat(verboseArgumentsReceived, "v");
                break;
            case 'V':
                printf("OwlZilla Client Version\n");
                printf("%.2f\n", version);
                exit(EXIT_SUCCESS);
            case '?':
                fprintf(stderr, "Refer to -h for help\n");
                return 1;
            default:
                abort();
        }
    }
    if (port == 0) {
        fprintf(stderr, "Port number is missing! Run this command with -p port#\n");
        exit(EXIT_FAILURE);
    }
    if (ipTemp[0] == 0) {
        fprintf(stderr, "Ip address is missing! Run this command with -i 127.0.0.1\n");
        exit(EXIT_FAILURE);
    }
    if (verbose) {
        printf("Verbose mode enabled!\n");
        printf("Arguments received: %s\n", verboseArgumentsReceived);
    }
    socketfd = ConnectToServer(ip, port);
    PromptForCommand(socketfd);
    return 0;
}

int ConnectToServer(struct in_addr ip, int port) {
    int socketfd;
    int connectionattempts = 0;
    struct sockaddr_in socketConnectionInfo;

    memset(&socketConnectionInfo, 0, sizeof(socketConnectionInfo));

    socketConnectionInfo.sin_addr = ip;
    socketConnectionInfo.sin_port = htons((uint16_t) port);
    socketConnectionInfo.sin_family = AF_INET;

    socketfd = socket(AF_INET, SOCK_STREAM, 0);
//    if (bind(socketfd, (struct sockaddr *)&socketConnectionInfo, sizeof(socketConnectionInfo)) < 0)
//    {
//        fprintf(stderr, "Failed binding to IP! Aborting.\n");
//        exit(EXIT_FAILURE);
//    }
//    else
//        if (verbose)
//            fprintf(stderr, "Binding IP to socket completed successfully!\n");

    while (connect(socketfd, (struct sockaddr *) &socketConnectionInfo, sizeof(socketConnectionInfo)) < 0) {
        ++connectionattempts;
        if (connectionattempts == 10) {
            fprintf(stderr, "Failed to connect after 10 attempts. Giving up.\n");
            exit(EXIT_FAILURE);
        }
        else
            fprintf(stderr, "Failed to connect to server!\n");
        sleep(1);
    }
    if (verbose)
        fprintf(stderr, "Successfully connected to server!\n");
    return socketfd;
}


void PromptForCommand(int socketfd) {
    bool quit = false;
    char cmd[1024] = {'\0 '};

    while (quit == false) {
        memset(cmd, 0, sizeof(cmd));
        printf("OwlZilla >> ");
        fgets(cmd, sizeof(cmd), stdin);
        quit = ProcessCommand(cmd, socketfd);
    }
}

bool ProcessCommand(char *cmd, int socketfd) {
    char cmdcopy[1024];
    char reading[4096];
    char *tok;

    strcpy(cmdcopy, cmd);

    tok = strtok(cmdcopy, " \n");
    if (tok == NULL) {
        //nothing
    }
    else if (strcmp(tok, "exit") == 0) {
        return true;
    }
    else if (strcmp(tok, "lpwd") == 0) {
        {
            char buf[100];
            memset(buf, 0, sizeof(buf));
            printf("Current Directory: %s\n", getcwd(buf, sizeof(char) * 100));
        }
    }
    else if (strcmp(tok, "pwd") == 0) {
        write(socketfd, cmd, sizeof(cmd) / sizeof(char));
        memset(cmd, 0, sizeof(cmd));
        read(socketfd, cmd, 1024);
        if (cmd[0] != NULL)
            printf("%s\n", cmd);
    }
    else if (strcmp(tok, "lcd") == 0) {
        tok = strtok(NULL, " \n");
        if (verbose)
            fprintf(stderr, "Token for lcd: %s\n", tok);
        if (tok == NULL) {
            if (verbose)
                fprintf(stderr, "lcd is going home!\n");
            chdir(getenv("HOME"));
        }
        else {
            if (verbose)
                fprintf(stderr, "lcd is going somewhere!\n");
            chdir(tok);
        }

    }
    else if (strcmp(tok, "cd") == 0) {
        write(socketfd, cmd, strlen(cmd) + 1);
    }
    else if (strcmp(tok, "lhome") == 0) {
        chdir(getenv("HOME"));
        printf("Set local cwd to home.\n");
    }
    else if (strcmp(tok, "home") == 0) {
        write(socketfd, cmd, strlen(cmd) + 1);
    }
    else if (strcmp(tok, "ldir") == 0) {
        FILE *tempfile = 0;
        size_t freadbytesread = 1;

        memset(reading, 0, 4096);

        //popen
        tempfile = popen("ls -lFABh --group-directories-first", "r");
        freadbytesread = fread(reading, sizeof(char), 4096, tempfile);
        while (freadbytesread != 0) {
            printf("%s", reading);
            freadbytesread = fread(reading, sizeof(char), 4096, tempfile);
            memset(reading, 0, 4096);
        }

        pclose(tempfile);
    }
    else if (strcmp(tok, "dir") == 0) {
        write(socketfd, cmd, strlen(cmd) + 1);
        memset(reading, 0, sizeof(reading));
        //while reading does not carry EOT_CHAR.
        while (memchr(reading, EOT_CHAR, sizeof(reading)) == NULL) {
            memset(reading, 0, sizeof(reading));
            read(socketfd, reading, 4096);
            printf("%s", reading);
        }
    }
    else if (strcmp(tok, "put") == 0) {
        //client is sending to server.
        struct stat filestat;
        ssize_t filesize = 0;
        char buf[4096] = { '\0' };
        memset(&filestat, 0, sizeof(filestat));
        tok = strtok(NULL, " \n");

        if (stat(tok, &filestat) == 0)
        {
            if (verbose)
                fprintf(stderr, "Successfully performed stat on file.\n");
            filesize = filestat.st_size;
            memset(buf, 0, sizeof(buf));
            sprintf(buf, "put %s %li", tok, filesize);
            if (verbose)
                fprintf(stderr, "Sending this string to server: %s\n", buf);
            write(socketfd, buf, strlen(buf) + 1);
            memset(buf, 0, sizeof(buf));
            read(socketfd, buf, 4096);
            if (buf[0] == '0')
            {
                fprintf(stderr, "Server failed to open file to create. Not attempting to send file.\n");
            }
            else if (buf[0] == '1')
            {
                if (verbose)
                    fprintf(stderr, "Server successfully opened file for creation. Continuing the file transfer.\n");
                {
                    int filefd = 0;
                    ssize_t i = 0;
                    ssize_t readi = 0;
                    filefd = open(tok, O_RDONLY);
                    if (filefd < 0)
                    {
                        fprintf(stderr, "Something real bad happened. Was successfully able to stat the file, but not open it.\n");
                        abort();
                    }
                    while (i < filesize)
                    {
                        memset(buf, 0, sizeof(buf));
                        readi = read(filefd, buf, 4096);
                        write(socketfd, buf, (size_t)readi);
                        i += readi;
                    }
                    close(filefd);
                    printf("File transfer succeeded!\n");
                }
            }
            else
            {
                fprintf(stderr, "Server is an unpredictable/undefined state.. Aborting to avoid crashing the server.\n");
                abort();
            }
        }
        else
        {
            fprintf(stderr, "Failed to open file!\n");
            if (verbose)
                fprintf(stderr, "Specifically, stat failed.\n");
        }



    }
    else if (strcmp(tok, "get") == 0) {
        //client is requesting file from server.
        int filefd = 0;
        char buf[4097] = { '\0' }; //one larger so strlen wont ever fail.
        char * temptok = NULL;
        ssize_t filesize = 0;

        tok = strtok(NULL, " \n");
        filefd = open(tok, O_CREAT | O_TRUNC | O_WRONLY, 0644);
        if (filefd < 0)
        {
            fprintf(stderr, "Failed to create file on client. Giving up on this request.\n");
            return false;
        }
        write(socketfd, cmd, strlen(cmd) + 1);
        read(socketfd, buf, 4096);
        temptok = strtok(buf, " ");
        if (temptok != NULL)
        {
            filesize = atol(temptok);
            temptok = strtok(NULL, " ");
            if (temptok != NULL)
            {
                write(filefd, temptok, strlen(temptok) + 1);
            }
            memset(buf, 0, sizeof(buf));
            if (filesize == 0)
            {
                fprintf(stderr, "File did not open on server. Giving up on this request.\n");
                close(filefd);
                return false;
            }
            else{
                ssize_t i = 0;
                ssize_t readi = 0;
                while (i < filesize)
                {
                    memset(buf, 0, sizeof(buf));
                    readi = read(socketfd, buf, 4096);
                    write(filefd, buf, (size_t)readi);
                    i += readi;
                }
            }
        }
        else
        {
            fprintf(stderr, "No fize size was retrieved from server! Giving up on this request.\n");
            close(filefd);
            return false;
        }
        close(filefd);
    }
    else if (strcmp(tok, "quitserver") == 0) {
        write(socketfd, cmd, strlen(cmd) + 1);
        return true;
    }
    else if (strcmp(tok, "help") == 0) {
        printf("OwlZilla Command Help:\n");
        printf("\thelp - This help message\n");
        printf("\tlcwd - Prints client's current working directory\n");
        printf("\tcwd - Prints server's current working directory\n");
        printf("\tlcd - Changes client's current working directory\n");
        printf("\tcd - Changes server's current working directory\n");
        printf("\texit - Exit's the client\n");
    }
    return false;
}




