//
// Created by tommydrum on 5/23/16.
//

#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <stdlib.h>
#include <getopt.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <pthread.h>
#include <signal.h>
#include <sys/types.h>
#include <fcntl.h>
#include <sys/stat.h>

#define EOT_CHAR '\04'
#define OPTIONS     "p:i:hvV"

bool verbose = false;
bool quitserver = false;
double version = 1.0;
pthread_mutex_t changingDir = PTHREAD_MUTEX_INITIALIZER;

int BindServerIP(int port, struct in_addr ip);

void AcceptConnection(int listenID);

void *ReadCommand(void *socketfd);

int main(int argc, char **argv) {
    //var declarations
    char verboseArgumentsReceived[6] = {'\0'};
    int listenfd = 0;
    int port = 0;
    char ipTemp[16] = {'\0'};
    struct in_addr ip;


    //code
    int c;
    while ((c = getopt(argc, argv, OPTIONS)) != -1) //p:i:hvV
    {
        switch (c) {
            case 'p':
                port = atoi(optarg);
                strcat(verboseArgumentsReceived, "p");
                break;
            case 'i':
                strcpy(ipTemp, optarg);
                if (inet_pton(AF_INET, ipTemp, &ip) != 1)
                    fprintf(stderr, "inet_pton failed!\n");
                else
                    fprintf(stderr, "inet-pton completed sucessfully\n");
                strcat(verboseArgumentsReceived, "i");
                break;
            case 'h':
                printf("OwlZilla Client Help: \n");
                printf("\t-p #####\tSpecify Port\n");
                printf("\t-i ip_addr\tSpecify IP Address\n");
                printf("\t-v\tVerbose mode\n");
                printf("\t-V\tVersion\n");
                printf("\t-h\tThis help message\n");
                exit(EXIT_SUCCESS);
            case 'v':
                verbose = true;
                strcat(verboseArgumentsReceived, "v");
                break;
            case 'V':
                printf("OwlZilla Server Version\n");
                printf("%.2f\n", version);
                exit(EXIT_SUCCESS);
            case '?':
                fprintf(stderr, "Refer to -h for help\n");
                return 1;
            default:
                abort();
        }
    }
    if (port == 0) {
        fprintf(stderr, "Port number is missing! Run this command with -p port#\n");
        exit(EXIT_FAILURE);
    }
    if (ipTemp[0] == 0) {
        fprintf(stderr, "Ip address is missing! Run this command with -i 127.0.0.1\n");
        exit(EXIT_FAILURE);
    }
    if (verbose) {
        printf("Verbose mode enabled!\n");
        printf("Arguments received: %s\n", verboseArgumentsReceived);
    }
    listenfd = BindServerIP(port, ip);
    while (quitserver == false) {
        AcceptConnection(listenfd);
    }

    return 0;
}

void AcceptConnection(int listenID) {
    int connectionfd = 0;
    pthread_t clientid;

    if (listen(listenID, 1) < 0) {
        fprintf(stderr, "Listen failed!\n");
    }
    else if (verbose) {
        fprintf(stderr, "Listening for connection...\n");
    }
    if ((connectionfd = accept(listenID, (struct sockaddr *) NULL, NULL)) < 0) {
        fprintf(stderr, "Accept failed!\n");
    }
    else if (verbose) {
        fprintf(stderr, "Accept returned a socket file descriptor!\n");
        fprintf(stderr, "Socket file descriptor value is: %d\n", connectionfd);
    }

    pthread_create(&clientid, NULL, ReadCommand, (void *) connectionfd);

}

int BindServerIP(int port, struct in_addr ip) {
    int listenfd;
    struct sockaddr_in socketConnectionData;

    listenfd = socket(AF_INET, SOCK_STREAM, 0);
    memset(&socketConnectionData, 0, sizeof(socketConnectionData));
    socketConnectionData.sin_family = AF_INET;
    //socketConnectionData.sin_addr = ptonl(INADDR_ANY);
    socketConnectionData.sin_addr = ip;
    socketConnectionData.sin_port = htons((uint16_t) port);

    if (bind(listenfd, (struct sockaddr *) &socketConnectionData, sizeof(socketConnectionData)) < 0) {
        fprintf(stderr, "Failed to bind IP! Aborting!\n");
        exit(EXIT_FAILURE);
    }
    else if (verbose)
        fprintf(stderr, "Successfully binded port!\n");

    return listenfd;
}

void *ReadCommand(void *socketfdv) {
    int socketfd = (int) socketfdv;
    char buf[1024];
    char temp[4096];
    char *tok = '\0';
    ssize_t bytesread = -1;
    char cwd[1024];
    char tempcwd[1024];

    pthread_detach(pthread_self());
    //defaults to home directory.
    strcpy(cwd, getenv("HOME"));

    while (bytesread != 0) {
        memset(buf, 0, sizeof(buf));
        bytesread = read(socketfd, buf, 1024);
        if (bytesread != 0) {
            if (verbose)
                fprintf(stderr, "[DEBUG] Received from client: %s %d\n", buf, (int) bytesread);
            tok = strtok(buf, " \n");
            if (tok == NULL) {

            }
            else if (strcmp(tok, "pwd") == 0) {
                write(socketfd, cwd, strlen(cwd));
            }
            else if (strcmp(tok, "cd") == 0) {
                tok = strtok(NULL, " \n");
                if (tok != NULL) {
                    if (tok[0] != '/') {
                        memset(tempcwd, 0, 1024);
                        strcpy(tempcwd, cwd);
                        strcat(tempcwd, "/");
                        strcat(tempcwd, tok);

                        if (chdir(tempcwd) == 0)
                            strcpy(cwd, getcwd(cwd, 1024));
                    }
                    else if (chdir(tok) == 0)
                        strcpy(cwd, getcwd(cwd, 1024));

                }
                else
                    strcpy(cwd, getenv("HOME"));
            }
            else if (strcmp(tok, "home") == 0) {
                strcpy(cwd, getenv("HOME"));
            }
            else if (strcmp(tok, "dir") == 0) {
                FILE *tempfile = 0;
                size_t freadbytesread = 1;
                if (pthread_mutex_lock(&changingDir) != 0) {
                    if (verbose)
                        fprintf(stderr, "Failed to DirectoryChange mutex...\n");
                }
                else if (verbose)
                    fprintf(stderr, "DirectoryChange Mutex locked successfully!\n");


                memset(temp, 4096, 0);
                chdir(cwd);

                //popen
                tempfile = popen("ls -lFABh --group-directories-first", "r");
                while (freadbytesread != 0) {
                    freadbytesread = fread(temp, sizeof(char), 4096, tempfile);
                    write(socketfd, temp, freadbytesread + 1);
                }
                temp[0] = EOT_CHAR;
                temp[1] = '\0';
                write(socketfd, temp, 2);
                pclose(tempfile);
                pthread_mutex_unlock(&changingDir);

            }
            else if (strcmp(tok, "get") == 0) {
                int getfd;
                char filename[2048] = {'\0'};
                char buf[4096];
                ssize_t filesize = 0;
                struct stat filestat;
                //server needs to send a file.
                tok = strtok(NULL, " \n");
                memset(&filestat, 0, sizeof(filestat));
                memset(buf, 0, sizeof(buf));
                strcpy(filename, cwd);
                strcat(filename, "/");
                strcat(filename, tok);
                getfd = open(filename, O_RDONLY);
                if (getfd < 0) {
                    sprintf(buf, "%li ", filesize);
                    write(socketfd, buf, strlen(buf) + 1); //size of file is 0 if it doesn't exist..
                }
                else {
                    //stat and send client the file size.
                    ssize_t i = 0;
                    ssize_t readi = 0;
                    fstat(getfd, &filestat);
                    filesize = filestat.st_size;
                    sprintf(buf, "%li ", filesize);
                    write(socketfd, buf, strlen(buf) + 1); //client now knows how large the file being sent is.
                    readi = read(getfd, buf, sizeof(buf));
                    while (i < filesize)
                    {
                        write(socketfd, buf, strlen(buf));
                        memset(buf, 0, sizeof(buf));
                        i += readi;
                        readi = read(getfd, buf, sizeof(buf));
                    }
                    close(getfd);
                }

            }
            else if (strcmp(tok, "put") == 0) {
                int putfd;
                char filename[2048] = {'\0'};
                char buf[4096] = { '\0' };
                ssize_t filesize = 0;
                //server needs to accept the file.
                tok = strtok(NULL, " \n");
                strcpy(filename, cwd);
                strcat(filename, "/");
                strcat(filename, tok);
                putfd = open(filename, O_CREAT | O_TRUNC | O_WRONLY, 0644);
                if (putfd < 0) {
                    fprintf(stderr, "A file failed to be created..\n");
                    write(socketfd, "0", 2);
                }
                else {
                    write(socketfd, "1", 2); //letting client know it's safe to start writing.
                    tok = strtok(NULL, " \n");
                    filesize = atol(tok);
                    {
                        ssize_t i = 0;
                        ssize_t tempi = 0;
                        while (i < filesize)
                        {
                            tempi = read(socketfd, buf, 4096);
                            write(putfd, buf, (size_t)tempi);
                            i += tempi;
                        }
                    }
                    close(putfd);
                }
            }
            else if (strcmp(tok, "quitserver") == 0) {
                raise(SIGKILL);
            }
        }
    }
    if (verbose)
        fprintf(stderr, "Socket %d is closed\n", socketfd);

    return NULL;
}

