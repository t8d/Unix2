/*
  Jesse Chaney

  A brief piece of demo code for CST240 Lab7
 */

#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>

#include "oitar.h"

int main(
  int argc
  , char *argv[]
)
{
  int fd = -1;
  ssize_t res;
  oitar_hdr_t hdr;
  size_t file_name_len;
  char *input_file_name = "demo5-1.txt";
  char file_name_len_char[OITAR_FNAME_LEN_SIZE + 1];
  char file_name[OITAR_MAX_FILE_NAME_LEN + 1];
  char file_size_char[OITAR_FILE_SIZE + 1];
  size_t file_size;

  // Open the file to read as input.
  fd = open(input_file_name, O_RDONLY);
  if (fd > 0) {

    // Read the entire structure from the file in one swell foop.
    while ((res = read(fd, (void *) &hdr, sizeof(oitar_hdr_t))) > 0) {
      // Make sure the contents of the file_name_char buffer are all 
      // zeroes (aka NULL).
      memset((void *) file_name_len_char, 0, sizeof(file_name_len_char));

      // Turning the hdr.oitar_fname_len field into a number takes a little bit
      // of work. First, we pull out those specific characters into an array of
      // of characters, which happens to also be NULL terminated, making it a
      // string. Notice the use of strncpy (note the N in the function name). Then 
      // use strtol() to turn the string into an integer. I could have used sscanf to
      // do this instead.
      // 
      // If I really wanted to live dangeriously, I could use fscanf to read the 
      // structure components from the file. If you REALLY want to do that, you are 
      // on your own.
      strncpy(file_name_len_char, hdr.oitar_fname_len, OITAR_FNAME_LEN_SIZE);
      file_name_len = (size_t) strtol(file_name_len_char, NULL, 10);

      // Make sure the contents of the file_name buffer are all zeroes (aka NULL).
      memset((void *) file_name, 0, sizeof(file_name));
      strncpy(file_name, hdr.oitar_fname, file_name_len);

      memset((void *) file_size_char, 0, sizeof(file_size_char));
      strncpy(file_size_char, hdr.oitar_size, OITAR_FILE_SIZE);
      file_size = (size_t) strtol(file_size_char, NULL, 10);

      printf("file name: >%s<  length: %u   file size: %u\n"
	     , file_name, (unsigned) file_name_len, (unsigned) file_size);
      if (argc > 1) {
	off_t offset;

	// If there is something on the command line, skip over each even entry
	// in the file, using a lseek() command.
	offset = lseek(fd, sizeof(oitar_hdr_t), SEEK_CUR);
	if (offset < 0) {
	  printf("error seeking in file\n");
	  exit(EXIT_FAILURE);
	}
      }
    }
  }
  else {
    printf("Failed to open file %s\n", input_file_name);
    exit(EXIT_FAILURE);
  }

  return(EXIT_SUCCESS);
}
