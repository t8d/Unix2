/*
  Jesse Chaney
 */

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>

#include "oitar.h"

int main(
  int argc
  , char *argv[]
)
{
  printf("Sizeof of oitar_hdr_t:     %5ld\n\n", (long) sizeof(oitar_hdr_t));

  printf("All offsets are zero based\n");

  printf("Offset of oitar_fname:     %5ld\n", (long) offsetof(oitar_hdr_t, oitar_fname) + 1);
  printf("        It is in positions %5ld to %5ld\n"
	 , (long) offsetof(oitar_hdr_t, oitar_fname) + 1, offsetof(oitar_hdr_t, oitar_fname_len));

  printf("\n");

  printf("Offset of oitar_fname_len: %5ld\n", (long) offsetof(oitar_hdr_t, oitar_fname_len) + 1);
  printf("        It is in positions %5ld to %5ld\n"
	 , (long) offsetof(oitar_hdr_t, oitar_fname_len) + 1, offsetof(oitar_hdr_t, oitar_adate));

  printf("Offset of oitar_adate:     %5ld\n", (long) offsetof(oitar_hdr_t, oitar_adate) + 1);
  printf("        It is in positions %5ld to %5ld\n"
	 , (long) offsetof(oitar_hdr_t, oitar_adate) + 1, offsetof(oitar_hdr_t, oitar_mdate));

  printf("Offset of oitar_mdate:     %5ld\n", (long) offsetof(oitar_hdr_t, oitar_mdate) + 1);
  printf("        It is in positions %5ld to %5ld\n"
	 , (long) offsetof(oitar_hdr_t, oitar_mdate) + 1, offsetof(oitar_hdr_t, oitar_uid));

  printf("Offset of oitar_uid:       %5ld\n", (long) offsetof(oitar_hdr_t, oitar_uid) + 1);
  printf("        It is in positions %5ld to %5ld\n"
	 , (long) offsetof(oitar_hdr_t, oitar_uid) + 1, offsetof(oitar_hdr_t, oitar_gid));
  printf("Offset of oitar_gid:       %5ld\n", (long) offsetof(oitar_hdr_t, oitar_gid) + 1);
  printf("        It is in positions %5ld to %5ld\n"
	 , (long) offsetof(oitar_hdr_t, oitar_gid) + 1, offsetof(oitar_hdr_t, oitar_mode));

  printf("Offset of oitar_mode:      %5ld\n", (long) offsetof(oitar_hdr_t, oitar_mode) + 1);
  printf("        It is in positions %5ld to %5ld\n"
	 , (long) offsetof(oitar_hdr_t, oitar_mode) + 1, offsetof(oitar_hdr_t, oitar_size));
  printf("Offset of oitar_size:      %5ld\n", (long) offsetof(oitar_hdr_t, oitar_size) + 1);
  printf("        It is in positions %5ld to %5ld\n"
	 , (long) offsetof(oitar_hdr_t, oitar_size) + 1, offsetof(oitar_hdr_t, oitar_hdr_end));

  printf("Offset of oitar_hdr_end:   %5ld\n", (long) offsetof(oitar_hdr_t, oitar_hdr_end) + 1);
  printf("        It is in positions %5ld to %5ld\n"
	 , (long) offsetof(oitar_hdr_t, oitar_hdr_end) + 1, sizeof(oitar_hdr_t));

  return(EXIT_SUCCESS);
}
