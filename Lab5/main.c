//
// Created by Tommy Miller on 2/24/16.
//
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>

extern void InsertIntoOitar(const char * oitarFile, char ** args, int vFlag);
extern void DeleteFromOitar(const char * oitarFile, char ** args, int vFlag);
extern void TableOfContentsShort(const char * oitarFile, int vFlag);
extern void TableOfContentsLong(const char * oitarFile, int vFlag);
extern void ExtractOitar(const char * oitarFile, char ** args, int vFlag);

#define OPTIONS    "adehtTvV"

int main(int argc, char ** argv) {
    //All of the command flags.
    int aFlag = 0; //Append flag
    int dFlag = 0; //Delete flag
    int eFlag = 0; //Extract flag
    int tFlag = 0; //Short archive display flag
    int TFlag = 0; //Long archive display flag
    int vFlag = 0; //Verbose flag

    int c; //getopt variable used variable.
    char * oitarFile = NULL;

    while ((c = getopt(argc, argv, OPTIONS)) != -1) //adehtTvV
    {
        switch (c)
        {
            case 'a': //add member(s)
                aFlag = 1;
                break;
            case 'd': //delete member(s)
                dFlag = 1;
                break;
            case 'e': //Extract member(s) from archive. (if no members are identified, extract everything)
                eFlag = 1;
                break;
            case 'h': //Show help text and exit.
                printf("\toitar <options> [archive-file] [member [...]]\n");
                printf("\t\toptions: adehtTvV\n\n");
                printf("\t\t-a Add member(s) from the command line to archive\n");
                printf("\t\t-d Delete member(s) from command line\n");
                printf("\t\t-e Extract member(s) from archive to file, from command line\n");
                printf("\t\t-h Show this help message and exit\n");
                printf("\t\t-t Short table of contents\n");
                printf("\t\t-T Long table of contents\n");
                printf("\t\t-v Verbose processing\n");
                printf("\t\t-V Show version and exit\n");
                return 0;
            case 't': //Short table of contents.
                tFlag = 1;
                break;
            case 'T': //Long table of contents.
                TFlag = 1;
                break;
            case 'v': //Verbose processing.
                vFlag = 1;
                break;
            case 'V': //Version information and exit.
                printf("\tOITAR\n");
                printf("\tby Tommy Miller\n\n");
                printf("\tVersion 0.01 InDev\n");
                return 0;
            case '?': //Missing arguments
                return 1;
            default:
                abort(); //never supposed to call this, but in case GetOpt gets a bad time from user.
        }
    }
    oitarFile = argv[optind];

    if (aFlag == 1)
    {
        InsertIntoOitar(oitarFile, &argv[optind + 1], vFlag);
        return 0;
    }
    if (tFlag == 1)
    {
        TableOfContentsShort(oitarFile, vFlag);
        return 0;
    }
    if (TFlag == 1)
    {
        TableOfContentsLong(oitarFile, vFlag);
        return 0;
    }
    if (eFlag == 1)
    {
        ExtractOitar(oitarFile, &argv[optind + 1], vFlag);
        return 0;
    }
    if (dFlag == 1)
    {
        DeleteFromOitar(oitarFile, &argv[optind + 1], vFlag);
        return 0;
    }

    return 0;
}
