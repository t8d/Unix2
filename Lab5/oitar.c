//
// Created by Tommy Miller on 2/24/16.
//
#include <stdlib.h>
#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <sys/stat.h>
#include "oitar.h"
void InsertIntoOitar(const char * oitarFile, char ** args, int vFlag);
void DeleteFromOitar(const char * oitarFile, char ** args, int vFlag);
void TableOfContentsShort(const char * oitarFile, int vFlag);
void TableOfContentsLong(const char * oitarFile, int vFlag);
void ExtractOitar(const char * oitarFile, char ** args, int vFlag);

void InsertIntoOitar(const char * oitarFile, char ** args, int vFlag)
{
    int i = 0;
    ssize_t y = 0;
    int didCreate = 0;
    int tempFile;
    struct stat fstatInfo;
    char buf[OITAR_MAX_MEMBER_FILE_SIZE];
    oitar_hdr_t tempOHDRFile;
    int oitar;
    oitar = open(oitarFile, O_WRONLY | O_APPEND);
    if (vFlag)
        fprintf(stderr, "In InsertIntoOitar.\n");
    if (vFlag)
        fprintf(stderr, "Attempting to open archive file.\n");
    if (oitar <= 0)
    {
        //try creating file.
        didCreate = 1;
        oitar = open(oitarFile, O_WRONLY | O_CREAT | O_APPEND, 0644);
        if (oitar <= 0)
        {
            //this is actually a bad failure.
            fprintf(stderr, "%s could not be opened in Read-Write mode!\n", oitarFile);
            exit(OITAR_FILE_COULD_NOT_OPEN);
        }
    }
    if (oitar > 0)
    {
        if (vFlag)
            fprintf(stderr, "Open success.\n");
        if (didCreate == 1)
        {
            //add in OITAR_ID
            write(oitar, OITAR_ID, OITAR_ID_LEN);

        }

        i = 0;
        memset(&fstatInfo, 0, sizeof(struct stat));
        memset(&tempOHDRFile, 0, sizeof(oitar_hdr_t));
        memset(&buf, 0, OITAR_MAX_MEMBER_FILE_SIZE);
        if (vFlag)
            fprintf(stderr, "Going into loop to insert each specified file\n");
        if (args == NULL || args[i] == NULL)
        {
            fprintf(stderr, "No file to append file with.\n");
        }
        while (args != NULL && args[i] != NULL)
        {
            if (vFlag)
                fprintf(stderr, "Opening file: %s\n", args[i]);
            tempFile = open(args[i], O_RDONLY);
            if (tempFile <= 0)
            {
                fprintf(stderr, "%s Input file could not be opened in Read-Only mode!\n", args[i]);
                exit(OITAR_FILE_COULD_NOT_OPEN);
            }
            else
            {
                if (vFlag)
                    fprintf(stderr, "Filling up header information\n");
                memset(&fstatInfo, 0, sizeof(fstatInfo));
                fstat(tempFile, &fstatInfo);
                //add file to the archive.
                memset(&tempOHDRFile, 0, sizeof(oitar_hdr_t));
                sprintf(tempOHDRFile.oitar_fname, "%-25s", args[i]);
                sprintf(tempOHDRFile.oitar_fname_len, "%2d", (int)strlen(args[i]));
                sprintf(tempOHDRFile.oitar_adate, "%10d", (int)fstatInfo.st_atime);
                sprintf(tempOHDRFile.oitar_mdate, "%10d", (int)fstatInfo.st_mtime);
                sprintf(tempOHDRFile.oitar_uid, "%-5d", fstatInfo.st_uid);
                sprintf(tempOHDRFile.oitar_gid, "%-5d", fstatInfo.st_gid);
                sprintf(tempOHDRFile.oitar_mode,"%6o", fstatInfo.st_mode);
                sprintf(tempOHDRFile.oitar_size, "%-12d", (int)fstatInfo.st_size);
                sprintf(tempOHDRFile.oitar_hdr_end, "%2s", OITAR_HDR_END);
                if (vFlag)
                    fprintf(stderr, "Writing header to archive\n");
                write(oitar, &tempOHDRFile, sizeof(tempOHDRFile));
                //write file
                if (vFlag)
                    fprintf(stderr, "Reading file to archive\n");
                y = read(tempFile, buf, OITAR_MAX_MEMBER_FILE_SIZE);
                write(oitar, buf, (size_t)y);
                if (vFlag)
                    fprintf(stderr, "Closing file\n");
                close(tempFile);
            }
            i++;
        }

        //int inFile = open();
        if (vFlag)
            fprintf(stderr, "Finished adding all requested files to archive, closing the archive.\n");
        close(oitar);
    }
}

void DeleteFromOitar(const char * oitarFile, char ** args, int vFlag) //Needs worked on.
{
    fprintf(stderr, "Not yet implemented.\n");
    exit(1);
}

void TableOfContentsShort(const char * oitarFile, int vFlag)
{
    char oitarID[OITAR_ID_LEN + 1];
    char fileNameLenChar[OITAR_FNAME_LEN_SIZE + 1];
    char fileName[OITAR_MAX_FILE_NAME_LEN + 1];
    char fileSizeChar[OITAR_FILE_SIZE + 1];
    int oitar;
    oitar_hdr_t hdr;
    size_t fileNameLen;
    size_t fileSize;
    off_t offset;

    if (vFlag)
        fprintf(stderr, "In TableOfContentsShort.\n");
    if (vFlag)
        fprintf(stderr, "Attempting to open archive file.\n");
    oitar = open(oitarFile, O_RDONLY);
    if (oitar <= 0)
    {
        fprintf(stderr, "%s could not be opened in Read-Only mode!\n", oitarFile);
        exit(OITAR_FILE_COULD_NOT_OPEN);
    }
    else
    {
        if (vFlag)
            fprintf(stderr, "Open success.\n");
        memset(oitarID, 0, sizeof(oitarID));
        if (vFlag)
            fprintf(stderr, "Attempting to read oitar id.\n");
        read(oitar, oitarID, OITAR_ID_LEN);

        if (strcmp(oitarID, OITAR_ID) != 0)
        {
            fprintf(stderr, "Not a valid oitar file. Aborting.\n");
            exit(OITAR_FILE_CORRUPT);
        }
        if (vFlag)
            fprintf(stderr, "Oitar ID matches OITAR_ID.\n");
        printf("Short table of contents for oitar archive file: %s\n", oitarFile);

        if (vFlag)
            fprintf(stderr, "Reading headers for files.\n");

        while ((read(oitar, (void *)&hdr, sizeof(oitar_hdr_t))) > 0)
        {
            memset((void *)fileNameLenChar, 0, sizeof(fileNameLenChar));
            strncpy(fileNameLenChar, hdr.oitar_fname_len, OITAR_FNAME_LEN_SIZE);
            fileNameLen = (size_t)atoi(fileNameLenChar);

            memset((void *) fileName, 0, sizeof(fileName));
            strncpy(fileName, hdr.oitar_fname, fileNameLen);

            memset((void *)fileSizeChar, 0, sizeof(fileSizeChar));
            strncpy(fileSizeChar, hdr.oitar_size, OITAR_FILE_SIZE);
            fileSize = (size_t)atoi(fileSizeChar);

            printf("\t%s\n", fileName);

            memset(&offset, -1, sizeof(off_t));
            offset = lseek(oitar, fileSize, SEEK_CUR);
            if (offset < 0)
            {
                fprintf(stderr, "Error seeking in file. Aborting.\n");
                exit(OITAR_FILE_CORRUPT);
            }

        }

        if (vFlag)
            fprintf(stderr, "Done reading and displaying information Closing file and returning..\n");
        close(oitar);
    }
}

void TableOfContentsLong(const char * oitarFile, int vFlag) //Needs worked on.
{ //need to modify adding long output.
    char oitarID[OITAR_ID_LEN + 1];
    char fileNameLenChar[OITAR_FNAME_LEN_SIZE + 1];
    char fileName[OITAR_MAX_FILE_NAME_LEN + 1];
    char fileSizeChar[OITAR_FILE_SIZE + 1];
    int oitar;
    oitar_hdr_t hdr;
    size_t fileNameLen;
    size_t fileSize;
    off_t offset;
    mode_t fileMode;
    char fileModeStr[20];
    char fileModeNumStr[5];

    if (vFlag)
        fprintf(stderr, "In TableOfContentsShort.\n");
    if (vFlag)
        fprintf(stderr, "Attempting to open archive file.\n");
    oitar = open(oitarFile, O_RDONLY);
    if (oitar <= 0)
    {
        fprintf(stderr, "%s could not be opened in Read-Only mode!\n", oitarFile);
        exit(OITAR_FILE_COULD_NOT_OPEN);
    }
    else
    {
        if (vFlag)
            fprintf(stderr, "Open success.\n");
        memset(oitarID, 0, sizeof(oitarID));
        if (vFlag)
            fprintf(stderr, "Attempting to read oitar id.\n");
        read(oitar, oitarID, OITAR_ID_LEN);

        if (strcmp(oitarID, OITAR_ID) != 0)
        {
            fprintf(stderr, "Not a valid oitar file. Aborting.\n");
            exit(OITAR_FILE_CORRUPT);
        }
        if (vFlag)
            fprintf(stderr, "Oitar ID matches OITAR_ID.\n");
        printf("Long table of contents for oitar archive file: %s\n", oitarFile);

        if (vFlag)
            fprintf(stderr, "Reading headers for files.\n");

        while ((read(oitar, (void *)&hdr, sizeof(oitar_hdr_t))) > 0)
        {

            //Gathering how long the file name length is.
            memset((void *)fileNameLenChar, 0, sizeof(fileNameLenChar));
            strncpy(fileNameLenChar, hdr.oitar_fname_len, OITAR_FNAME_LEN_SIZE);
            fileNameLen = (size_t)atoi(fileNameLenChar);

            //Gathering file name (using the file name length to specify the length.
            memset((void *) fileName, 0, sizeof(fileName));
            strncpy(fileName, hdr.oitar_fname, fileNameLen);

            //Gathering the size of the file.
            memset((void *)fileSizeChar, 0, sizeof(fileSizeChar));
            strncpy(fileSizeChar, hdr.oitar_size, OITAR_FILE_SIZE);
            fileSize = (size_t)atoi(fileSizeChar);

            //Gathering file mode.
            strncpy(fileModeNumStr, (hdr.oitar_mode + 2), 4);
            memset(&fileMode, 0, sizeof(mode_t));
            memset(&fileModeStr, 0, sizeof(fileModeStr));
            fileMode = (mode_t)strtol(fileModeNumStr, NULL, 8);

            //Creating mode string.
            //User
            strcat(fileModeStr, (fileMode & S_IRUSR) ? "r" : "-");
            strcat(fileModeStr, (fileMode & S_IWUSR) ? "w" : "-");
            strcat(fileModeStr, (fileMode & S_IXUSR) ? "x" : "-");
            //Group
            strcat(fileModeStr, (fileMode & S_IRGRP) ? "r" : "-");
            strcat(fileModeStr, (fileMode & S_IWGRP) ? "w" : "-");
            strcat(fileModeStr, (fileMode & S_IXGRP) ? "x" : "-");
            //World
            strcat(fileModeStr, (fileMode & S_IROTH) ? "r" : "-");
            strcat(fileModeStr, (fileMode & S_IWOTH) ? "w" : "-");
            strcat(fileModeStr, (fileMode & S_IXOTH) ? "x" : "-");

            printf("    File name: %s\n", fileName);
            printf("        File size:\t%d bytes\n", (int)fileSize);
            printf("        Permissions: \t%s\t(%s)\n", fileModeStr, fileModeNumStr); //Needs rw-r--r-- syntax then (0644)
            printf("        File owner:\t \t \n"); //needs evaluated username and UID
            printf("        File group:\t \t \n"); //needs evaluated group and GID
            printf("        Access date:\t \n"); //needs date, time, day of week, and raw data.
            printf("        Modify date:\t \n"); //Same as above.

            memset(&offset, 0, sizeof(off_t));
            offset = lseek(oitar, fileSize, SEEK_CUR);
            if (offset < 0)
            {
                fprintf(stderr, "Error seeking in file. Aborting.\n");
                exit(OITAR_FILE_CORRUPT);
            }

        }

        if (vFlag)
            fprintf(stderr, "Done reading and displaying information Closing file and returning..\n");
        close(oitar);
    }
}

void ExtractOitar(const char * oitarFile, char ** args, int vFlag) //Needs worked on.
{
    ssize_t errflag;
    char tag[OITAR_ID_LEN + 1];
    char * buf;
    size_t fileSize;
    size_t fileNameLen;
    int oitar;
    int tempFile;
    char fileNameLenChar[OITAR_FNAME_LEN_SIZE + 1];
    char fileName[OITAR_MAX_FILE_NAME_LEN + 1];
    char fileSizeChar[OITAR_FILE_SIZE + 1];
    off_t offset;

    oitar_hdr_t tempHeader;
    if (vFlag)
    {
        fprintf(stderr, "In ExtractOitar function.\n");
        fprintf(stderr, "Attempting to open OITAR with readonly\n");
    }
    oitar = open(oitarFile, O_RDONLY);
    if (oitar == -1)
    {
        fprintf(stderr, "Failed to open file. Aborting.\n");
        exit(OITAR_FILE_COULD_NOT_OPEN);
    }
    if (vFlag)
    {
        fprintf(stderr, "Checking !<oitar> flag\n");
    }
    memset(&tag, 0, OITAR_ID_LEN);
    errflag = read(oitar, tag, OITAR_ID_LEN);
    if ((errflag > 0) && strncmp(OITAR_ID, tag, OITAR_ID_LEN) == 0)
    {
        if (vFlag)
            fprintf(stderr, "!<oitar> tag found, continuing\n");
        if (*args == NULL) //extract all
        {
            memset(&tempHeader, 0, sizeof(oitar_hdr_t));
            while ((errflag = read(oitar, &tempHeader, sizeof(oitar_hdr_t))) > 0)
            {
                //Process header to write new file.
                memset((void *)fileNameLenChar, 0, sizeof(fileNameLenChar));
                strncpy(fileNameLenChar, tempHeader.oitar_fname_len, OITAR_FNAME_LEN_SIZE);
                fileNameLen = (size_t)atoi(fileNameLenChar);

                memset((void *) fileName, 0, sizeof(fileName));
                strncpy(fileName, tempHeader.oitar_fname, fileNameLen);

                memset((void *)fileSizeChar, 0, sizeof(fileSizeChar));
                strncpy(fileSizeChar, tempHeader.oitar_size, OITAR_FILE_SIZE);
                fileSize = (size_t)atoi(fileSizeChar);

                tempFile = open(fileName, O_WRONLY | O_CREAT | O_EXCL, 0644);
                if (tempFile == -1)
                {
                    fprintf(stderr, "%s could not be created. Does it already exist?\n", fileName);
                    //skipping write, lseeking ahead to next header.
                    memset(&offset, -1, sizeof(off_t));
                    offset = lseek(oitar, fileSize, SEEK_CUR);
                    if (offset < 0)
                    {
                        fprintf(stderr, "Error seeking in file. Aborting.\n");
                        exit(OITAR_FILE_CORRUPT);
                    }
                }
                else
                {
                    //Write file
                    buf = (char *)calloc(fileSize + 1, sizeof(char));

                    if (read(oitar, buf, fileSize) > 0)
                    {
                        write(tempFile, buf, fileSize);
                    }
                    else
                    {
                        fprintf(stderr, "Read failed! Aborting.\n");
                        exit(OITAR_FILE_CORRUPT);
                    }
                    close(tempFile);
                    free(buf);
                }
                memset(&tempHeader, 0, sizeof(oitar_hdr_t));
            }
            if (errflag < 0)
            {
                fprintf(stderr, "Read failed. Aborting\n");
                exit(OITAR_FILE_CORRUPT);
            }
        }
        else //extract specified file
        {
            fprintf(stderr, "Extract specified file(s) feature is not yet implemented, sorry.\n");
            exit(1);
        }
    }
    else if (errflag <= 0)
    {
        fprintf(stderr, "Read error!\n");
        exit(1);
    }
    else
    {
        fprintf(stderr, "Not a valid OITAR File!\n");
        exit(1);
    }
    close(oitar);
}