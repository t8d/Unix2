/*
 * R Jesse Chaney
 *
 * oitar.h
 */

#pragma once

# include <string.h>
# include <sys/cdefs.h>

/*
 * oitar archive files start with the OITAR_ID identifying string. Then follows a
 * 'struct oitar_hdr_s', and as many bytes of member file data as its 'oitar_size'
 * member indicates, for each member file.
 */

# define OITAR_ID	"!<oitar>\n"    /* String that begins an oitar archive file. */
# define OITAR_ID_LEN	9               /* Size of the oitar id string string. */

# define OITAR_HDR_END	"=\n"		/* String at end of each member header. */
# define OITAR_HDR_END_LEN  2

# define OITAR_MAX_FILE_NAME_LEN            25
# define OITAR_FNAME_LEN_SIZE                2
# define OITAR_DATE_SIZE	            10
# define OITAR_GUID_SIZE	             5
# define OITAR_MODE_SIZE	             6
# define OITAR_FILE_SIZE	            12
# define OITAR_MAX_MEMBER_FILE_SIZE    1000000 /* Just make things a little easier. */

// Some exit() codes.
# define OITAR_FILE_COULD_NOT_OPEN     2
# define OITAR_FILE_CORRUPT            3
# define OITAR_FILE_MEMBER_BAD         4
# define OITAR_FILE_AR_WRITE_FAILED    5
# define OITAR_FILE_MEM_FILE_OPEN_FAIL 6
# define OITAR_FILE_MEM_FILE_READ_FAIL 7
# define OITAR_FILE_AR_FILE_WRITE_FAIL 8

typedef struct oitar_hdr_s
{
    char oitar_fname[OITAR_MAX_FILE_NAME_LEN];   /* Member file name, may not be NULL terminated. */
    char oitar_fname_len[OITAR_FNAME_LEN_SIZE];  /* The length of the member file name */

    char oitar_adate[OITAR_DATE_SIZE];	         /* File access date, decimal sec since Epoch. */
    char oitar_mdate[OITAR_DATE_SIZE];	         /* File modify date, decimal sec since Epoch. */

    char oitar_uid[OITAR_GUID_SIZE];	         /* user id in ASCII decimal */
    char oitar_gid[OITAR_GUID_SIZE];             /* group id in ASCII decimal. */

    char oitar_mode[OITAR_MODE_SIZE];	         /* File mode, in ASCII octal. */
    char oitar_size[OITAR_FILE_SIZE];	         /* File size, in ASCII decimal. */

    char oitar_hdr_end[OITAR_HDR_END_LEN];       /* Always contains OITAR_HDR_END. */
} oitar_hdr_t;
