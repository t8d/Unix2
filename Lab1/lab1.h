// R. jesse Chaney


#define YOUR_NAME "Thomas Miller"

typedef struct account_s {
  int account_number;
  char first_name[50];
  char last_name[50];
  float balance;
} account_t;
