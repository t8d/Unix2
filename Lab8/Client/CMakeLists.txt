cmake_minimum_required(VERSION 3.5)
project(owlzillac)

set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -Wall -Wshadow -Wunreachable-code -Wredundant-decls -Wmissing-declarations -Wold-style-definition -Wmissing-prototypes -Wdeclaration-after-statement")

set(SOURCE_FILES owlzillac.c)
add_executable(owlzillac ${SOURCE_FILES})