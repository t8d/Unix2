//
// Created by tommydrum on 5/23/16.
//

#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <stdlib.h>
#include <getopt.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <pthread.h>

#define OPTIONS     "p:i:hvV"

bool verbose = false;
bool quitserver = false;
double version = 1.0;

int BindServerIP(int port, struct in_addr ip);
void AcceptConnection(int listenID);

void * ReadCommand(void * socketfd);

int main(int argc, char ** argv)
{
    //var declarations
    char verboseArgumentsReceived[6] = { '\0' };
    int listenfd = 0;
    int port = 0;
    char ipTemp[16] = { '\0' };
    struct in_addr ip;

    //code
    int c;
    while ((c = getopt(argc, argv, OPTIONS)) != -1) //p:i:hvV
    {
        switch(c)
        {
            case 'p':
                port = atoi(optarg);
                strcat(verboseArgumentsReceived, "p");
                break;
            case 'i':
                strcpy(ipTemp, optarg);
                if (inet_pton(AF_INET, ipTemp, &ip) != 1)
                    fprintf(stderr, "inet_pton failed!\n");
                else
                    fprintf(stderr, "inet-pton completed sucessfully\n");
                strcat(verboseArgumentsReceived, "i");
                break;
            case 'h':
                printf("OwlZilla Client Help: \n");
                printf("\t-p #####\tSpecify Port\n");
                printf("\t-i ip_addr\tSpecify IP Address\n");
                printf("\t-v\tVerbose mode\n");
                printf("\t-V\tVersion\n");
                printf("\t-h\tThis help message\n");
                exit(EXIT_SUCCESS);
            case 'v':
                verbose = true;
                strcat(verboseArgumentsReceived, "v");
                break;
            case 'V':
                printf("OwlZilla Server Version\n");
                printf("%.2f\n", version);
                exit(EXIT_SUCCESS);
            case '?':
                fprintf(stderr, "Refer to -h for help\n");
                return 1;
            default:
                abort();
        }
    }
    if (port == 0)
    {
        fprintf(stderr, "Port number is missing! Run this command with -p port#\n");
        exit(EXIT_FAILURE);
    }
    if (ipTemp[0] == 0)
    {
        fprintf(stderr, "Ip address is missing! Run this command with -i 127.0.0.1\n");
        exit(EXIT_FAILURE);
    }
    if (verbose)
    {
        printf("Verbose mode enabled!\n");
        printf("Arguments received: %s\n", verboseArgumentsReceived);
    }
    listenfd = BindServerIP(port, ip);
    while (quitserver == false)
    {
        AcceptConnection(listenfd);
    }

    return 0;
}

void AcceptConnection(int listenID)
{
    int connectionfd = 0;
    pthread_t clientid;

    if (listen(listenID, 1) < 0)
    {
        fprintf(stderr, "Listen failed!\n");
    }
    else
    if (verbose)
    {
        fprintf(stderr, "Listening for connection...\n");
    }
    if ((connectionfd = accept(listenID, (struct sockaddr *)NULL, NULL)) < 0)
    {
        fprintf(stderr, "Accept failed!\n");
    }
    else
    if (verbose)
    {
        fprintf(stderr, "Accept returned a socket file descriptor!\n");
        fprintf(stderr, "Socket file descriptor value is: %d\n", connectionfd);
    }

    pthread_create(&clientid, NULL, ReadCommand, (void *)connectionfd);

}

int BindServerIP(int port, struct in_addr ip)
{
    int listenfd;
    struct sockaddr_in socketConnectionData;

    listenfd = socket(AF_INET, SOCK_STREAM, 0);
    memset(&socketConnectionData, 0, sizeof(socketConnectionData));
    socketConnectionData.sin_family = AF_INET;
    //socketConnectionData.sin_addr = ptonl(INADDR_ANY);
    socketConnectionData.sin_addr = ip;
    socketConnectionData.sin_port = htons((uint16_t)port);

    if (bind(listenfd, (struct sockaddr *)&socketConnectionData, sizeof(socketConnectionData)) < 0)
    {
        fprintf(stderr, "Failed to bind IP! Aborting!\n");
        exit(EXIT_FAILURE);
    }
    else
        if (verbose)
           fprintf(stderr, "Successfully binded port!\n");

    return listenfd;
}

void * ReadCommand(void *socketfdv)
{
    int socketfd = (int)socketfdv;
    char buf[1024];
    char * tok = '\0';
    ssize_t bytesread = -1;
    char cwd[1024];
    char tempcwd[1024];

    pthread_detach(pthread_self());
    getcwd(buf, 1024);
    strcpy(cwd, buf);

    while (bytesread != 0)
    {
        memset(buf, 0, sizeof(buf));
        bytesread = read(socketfd, buf, 1024);
        if (bytesread != 0)
        {
            if (verbose)
                fprintf(stderr, "[DEBUG] Received from client: %s %d\n", buf, (int)bytesread);
            tok = strtok(buf, " \n");
            if (strcmp(tok, "pwd") == 0)
            {
                write(socketfd, cwd, strlen(cwd));
            }
            else if (strcmp(tok, "cd") == 0)
            {
                tok = strtok(NULL, " \n");
                if (tok != NULL)
                {
                    if (tok[0] != '/')
                    {
                        memset(tempcwd, 0, 1024);
                        strcpy(tempcwd, cwd);
                        strcat(tempcwd, "/");
                        strcat(tempcwd, tok);

                        if (chdir(tempcwd) == 0)
                            strcpy(cwd, getcwd(cwd, 1024));
                    }
                    else
                        if (chdir(tok) == 0)
                           strcpy(cwd, getcwd(cwd, 1024));

                }
                else
                    strcpy(cwd, getenv("HOME"));
            }
        }
    }
    if (verbose)
        fprintf(stderr, "Socket %d is closed\n", socketfd);

    return NULL;
}

