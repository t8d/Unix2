cmake_minimum_required(VERSION 3.5)
project(owlzillas)

set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -pthread -Wall -Wshadow -Wunreachable-code -Wredundant-decls -Wmissing-declarations -Wold-style-definition -Wmissing-prototypes -Wdeclaration-after-statement")

set(SOURCE_FILES owlzillas.c)
add_executable(owlzillas ${SOURCE_FILES})
