cmake_minimum_required(VERSION 3.5)
project(Lab4)

set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -Wall -g")

set(SOURCE_FILES main.c binary_tree.c binary_tree.h)
add_executable(Lab4 ${SOURCE_FILES})