/*
 * R Jesse Chaney
*/

typedef struct item_s {
    char *word;
    int count;
    struct item_s *left;
    struct item_s *right;
} item_t;


// Insert a new word into the binary tree.
// If the tree == NULL, create a new item and return that as the list.
// If the word already exists in the binary tree, increment the count for
//   the word, but don't insert a duplicate.
// Use the compare function to determine if 2 words are equal, greater than,
//   or less than each other.
item_t *insert_item(item_t *tree, char *word, int (compare)(void *, void *));

// Check to see if the word parameter exists in the binary tree. If it does,
//   return a pointer to the item. If it does not, return NULL.
item_t *locate_item(item_t *tree, char *word, int (compare)(void *, void *));

// Free all memory used by the binary tree.
void free_tree(item_t *tree);

// Display each word in the binary tree and show the count of the number of
//   times the word has been seen. The list of words should be in alphabetical
//   order.
void display_in_order(item_t *tree);

// Display each word in the binary tree and show the count of the number of
//   times the word has been seen. The list of words should be in reverse
//   alphabetical order.
void display_reverse_order(item_t *tree);
