/*
 * R Jesse Chaney
*/

// You can run the program in any of the following ways:
//     binary_tree words1.txt
//     binary_tree < words1.txt
//     cat words1.txt words2.txt | binary_tree

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include "binary_tree.h"

static int compare_words(void *word1, void *word2);

int main(
        int argc,
        char *argv[]
) {
  char word[100]; // I assume that no word has more than 99 characters.
  item_t *tree = NULL;
  FILE *file = 0;

  if (argc < 2) {
    // If argc is < 2, then the input must be read from stdin.
    file = stdin;
  }
  else {
    // Input is from a file listed on the command line.
    file = fopen(argv[1], "r");
  }

  // Read the input until EOF. Insert or update the count of
  //   each word in the binary tree.
  // Each line in the input is a single word.
  while ((fgets(word, sizeof(word), file)) > 0) {
    word[strcspn(word, "\n")] = 0;
    tree = insert_item(tree, word, compare_words);
  }
  fclose(file);

  // After you have built your binary tree, you can call these functions
  //   to display the contents of the tree.
  // You may want to comment or #ifdef these calls out until you have your
  //   binary tree created correctly.
  fprintf(stdout, "Displaying all items from tree in order\n");
  display_in_order(tree);

  fprintf(stdout, "\n");
  fprintf(stdout, "Displaying all items from tree in reverse order\n");
  display_reverse_order(tree);

  // Look for a specific word in the tree. Pass 3 arguments:
  //    1) the tree to be searched
  //    2) the word to find
  //    3) the function to compare to words
  //
  // I'm being lazy and looking for whatever was the last word read from
  // the input file.
  {
    item_t *item = NULL;

    printf("\n");
    item = locate_item(tree, word, compare_words);
    if (NULL == item) {
      printf("Unable to locate %s\n", word);
    }
    else {
      printf("Located word: %s   count: %d\n"
              , item->word, item->count);
    }

#define NOT_FOUND_WORD "__xYzzY__"
    // This word should never be found in the tree.
    item = locate_item(tree, NOT_FOUND_WORD, compare_words);
    if (NULL == item) {
      printf("Unable to locate %s\n", NOT_FOUND_WORD);
    }
    else {
      printf("Located word: %s   count: %d\n"
              , item->word, item->count);
    }
  }

  // Free all the memory related to a binary tree.
  free_tree(tree);

  return(0);
}

// A simple fuction that will compare 2 words from the tree and
// return a 0 if the words are the same. This fuction is very
// simple. It is just an example of how to do this sort of thing.
static int compare_words(void *word1, void *word2)
{
  return strcmp(word1, word2);
}
