import sys
import argparse

parser = argparse.ArgumentParser()

parser.add_argument('file', metavar='file', type=file, help='Input File Name')
parser.add_argument('integer', metavar='n', type=int, help='Number of digits to sum')

args = parser.parse_args()

inTempFile = args.file.read()
inFile = ''.join(inTempFile.split())
inNum = args.integer

out = 0
outstr = ''
temp = 0

for x in range(0, len(inFile) - inNum):
    m_slice = inFile[x:x+inNum]
    temp = 1
    for y in m_slice:
        temp *= int(y, 16)
    if temp > out:
        out = temp
        outstr = m_slice

print out, outstr
