#define LINE_MAX 1024

typedef struct list_s {
  char *line;
  struct list_s *next;
} list_t;
