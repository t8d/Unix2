#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "lab3.h"

int main()
{
    list_t *travel = NULL;
    list_t *head = NULL;
    char buf[4096] = { '\0' };
    travel = NULL;
    //loop 1, fills linked list with stdin's data.
    if  (fgets(buf, 4096, stdin) != NULL)
    {
        head = (list_t *)calloc(1, sizeof(list_t));
        travel = head;
        travel->line = strdup(buf);
        travel->next = NULL;
    }
    while (fgets(buf, 4096, stdin) != NULL)
    {
        travel->next = (list_t *) calloc(1, sizeof(list_t));
        travel = travel->next;
        travel->line = strdup(buf);
        travel->next = NULL;
    }

    //loop 2, prints each node's data.
    travel = head;
    int num = 0;
    while (travel != NULL)
    {
        printf("%03i: %s",num++, travel->line);
        travel = travel->next;
    }

    //loop 3, cleans up linked list.
    travel = head;
    list_t *temp = NULL;
    while (travel != NULL)
    {
        temp = travel->next;
        free(travel->line);
        free(travel);
        travel = temp;
    }
    return 0;
}

