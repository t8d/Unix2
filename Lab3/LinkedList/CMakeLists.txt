cmake_minimum_required(VERSION 3.5)
project(Lab3LinkedList)

set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -Wall -Werror")

set(SOURCE_FILES main.c lab3.h)
add_executable(Lab3LinkedList ${SOURCE_FILES})