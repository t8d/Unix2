#!/bin/bash
argm() 
{
    counter=0
    for x in `find /proc -ignore_readdir_race -maxdepth 2 $DEFINEUSER -name status`
    do
        grep -s '^Name:\|^State:\|^Threads:' $x | tr '\n' '\t'
        echo
        counter=$((counter + 1))
    done
    echo
    echo "Found $counter processes for $DEFINEUSER"
    exit

}

argh()
{
    echo "Find Proccess help"
    echo "-h             This help message"
    echo "-m             Display processes created by current user"
    echo "-u <user_name> Displays process created by said user"
    exit
}

#Program will start here (first command that's snot a function)
while getopts "hmu:" opt; do
    case $opt in
        h) #Help
            argh
            ;;
        m) #Me
            DEFINEUSER="-user ${USER}"
            argm
            ;;
        u) #User
            DEFINEUSER="-user ${OPTARG}"
            argm
            ;;
        \?)#Unknown
            echo "Unknown arguments!"
            argh
            ;;
        
        esac
    done

argm
