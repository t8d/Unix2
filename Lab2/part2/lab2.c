#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main()
{
    char ** fInput = NULL;
    int fInputSize = 0;
    char buffer[128] = { '\0' };
    while ((fgets(buffer, 127, stdin)) != NULL)
    {
        fInput = (char**)realloc(fInput, (sizeof(char *)) * (fInputSize + 1));
        fInput[fInputSize] = calloc(1, (sizeof(char) * (strlen(buffer) + 1)));
        strcpy(fInput[fInputSize++], buffer);
    }
    {
        int i;
        for(i = 0; i < fInputSize; ++i)
        {
            printf(" %i:\t%s", i, fInput[i]);
            free(fInput[i]);
        }
        free(fInput);
    }
    return 0;
}
