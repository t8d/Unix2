/***********************************************************
* Author:				Thomas Miller
* Date Created:			5/9/2016
* Last Modification Date:	5/12/2016
* Lab Number:			CST 250 UNIX
* Filename:				owlsh.c
*
* Overview:
*	This program will read in a command from the user,
*	determine if its an internal command, or something
*	to pass to execvp, and execute said command.
*
* Input:
*	Shell command/program.
*	EX:
*	cd
*	cd /home/
*	cd ~/Sources/
*	ls > file1.txt
*	cat < file1.txt
*	ls | cat
*
* Output:
*	The output is completely dependant on what
*	program/commands the user calls. And since
*	this variable is off the system/user path
*	environment variables, there's no telling
*	the output for every program.
************************************************************/

//Includes
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <string.h>
#include <fcntl.h>

//global variable (static) so the signal handler can know the PID of the child.
static pid_t cpid = -1;

//Function declarations
int GetCommand(void);
void SigHandler(int sig);
int ExecCommand(char **command, int input, int output, int pipes[2]);
void Empty(char ***command);

/**************************************************************
*	  Purpose:  Starts the signal handler, and loops the GetCommand
*
*     Entry:  None. The shell was called by user.
*
*     Exit:  0. The shell was exited by user.
****************************************************************/
int main(int argc, char* argv[])
{
	int loop;
	signal(SIGCHLD, SigHandler); //easy automatic child reapers.
	//loops if not child, or exit was not called.
	loop = 0;
	while (loop == 0)
	{
		loop = GetCommand();
		//Loop has 2 different possibilities.
		//0 is regular parent.. Keep looping.
		//1 is exit condition from parent. Exit the loop.
	}

	return 0;
}

/**************************************************************
*	  Purpose:  Prompt user for input, and parse the input.
*
*     Entry:  None. This is called in a loop.
*
*     Exit:  User input gets called into ExecCommand to get
*            executed.
****************************************************************/
int GetCommand(void)
{
	int i = 0;
	int numOfPipes = 0;
	char command[1024];
	char *token = NULL;
	int input = 0;
	int output = 0;
	int pipeInfo[2] = { 0 };
	int pipeInfo2[2] = { 0 };
	int loop;
	int pipeerror;
	char ** cmd;
	cmd = calloc(1, sizeof(char *));
	printf("owlsh %s>> ", getenv("USER"));
	memset(command, 0, sizeof(command));
	fgets(command, sizeof(command), stdin);
	token = strtok(command, " \n");
	while (token != NULL)
	{
		switch (*token)
		{
			case '<':
				token = strtok(NULL, " \n");
				input = open(token, O_RDONLY);
				if (input == -1)
				{
					printf("File does not exist!\n");
					Empty(&cmd);
					return 0; //exits command, ready a prompt for user's next input.
				}
				token = strtok(NULL, " \n");
				break;
			case '>':
				token = strtok(NULL, " \n");
				output = open(token, O_WRONLY | O_APPEND | O_CREAT, 0644);
				if (output == -1)
				{
					printf("Opening file to output failed. Aborting command.\n");
					Empty(&cmd);
					return 0; //failure state, but continue running the shell.
				}
				token = strtok(NULL, " \n");
				break;
			case '|':
				numOfPipes++;
				if (numOfPipes == 1) {
					pipeerror = pipe(pipeInfo);
					if (pipeerror < 0) {
						fprintf(stderr, "Pipe failed! Aborting command.\n");
						return 0;
					}
				}
				if (numOfPipes == 1)
				{
					ExecCommand(cmd, input, pipeInfo[1], 0);
					input = pipeInfo[0];
				}
				else if (numOfPipes % 2 == 0) //every other
				{
					//ExecCommand(cmd, input, pipeInfo[1], pipeInfo); //input in here would cause it to hang.
					pipe(pipeInfo2);
					ExecCommand(cmd, input, pipeInfo2[1], pipeInfo); //new output goes to pipeInfo2.
					input = pipeInfo2[0];
				}
				else
				{
					pipe(pipeInfo); //switches and uses the other (now closed) pipe.
					ExecCommand(cmd, input, pipeInfo[1], pipeInfo2);
					input = pipeInfo[0];
				}

				Empty(&cmd);
				cmd = calloc(1, sizeof(char *));
				i = 0;
				token = strtok(NULL, " \n");
				break;
			default:
				cmd = realloc(cmd, sizeof(char *) * (i + 1));
				cmd[i] = calloc(1, sizeof(token) + 1);
				strcpy(cmd[i], token);
				token = strtok(NULL, " \n");
				i += 1;
		}
	}
	loop = ExecCommand(cmd, input, output, numOfPipes == 0 || numOfPipes % 2 == 1 ? pipeInfo : pipeInfo2);
	if (input != 0 && input != pipeInfo[0])
	{
		close(input);
	}
	if (output != 0 && output != pipeInfo[1])
	{
		close(output);
	}
	Empty(&cmd);
	return loop;
}

/**************************************************************
*	  Purpose:  Empty a ragged array.
*
*     Entry:  A char ** array carrying a ragged array.
*
*     Exit:  All memory from the ragged array was freed.
****************************************************************/
void Empty(char ***command)
{
	int i = 0;
	while (((*(command)))[i] != NULL)
	{
		free(((*(command)))[i]);
		i++;
	}
	free(*(command));
	*command = NULL;
}

/**************************************************************
*	  Purpose:  Handles signals into program.
*
*     Entry:  SIGINT or SIGCHLD was thrown to process.
*
*     Exit:  If SIGCHLD, wait is called for that child,
*            If SIGINT, SIGINT gets forwarded to the child
*               process.
****************************************************************/
void SigHandler(int sig)
{
	int status = 0;
	if (sig == SIGCHLD) {
		wait(&status);
		printf("Child exited with error code %d\n", status);
	}
	else if (sig == SIGINT)
	{
		kill(cpid, SIGINT); //Forwards signal to child.
	}
}

/**************************************************************
*	  Purpose:  Executes commands/programs. If internal, command
*	    gets handled in respective if/else strcmp values.
*	    else (external program), the process forks, and the child
*	    is given to execvp with the data.
*
*     Entry:  command carries a ragged array of the program to be
*       executed.
*
*     Exit:  Program/command was executed, or returned to user:
*       unknown command.
****************************************************************/
int ExecCommand(char **command, int input, int output, int pipes[2])
{
	cpid = -1;
	if (command == NULL || command[0] == NULL)
	{
		//nothing
	}
	else if ( strcmp(command[0], "exit") == 0 )
	{
		printf("Exiting. \n");
		return 1;
	}
	else if ( strcmp(command[0], "help") == 0)
	{
		printf("\tOwlsh Help:\n");
		printf("\t\texit - Exit's out of the Owlsh Shell\n");
		printf("\t\tpwd - Print's the working directory\n");
		printf("\t\thelp - Displays this help message.\n\n");
		printf("\t\telse - Executes command.\n\n");
		printf("\t\tNote: This shell supports file input/output as < and > in the shell.\n");
		printf("\t\tPipes: This shell supports piping outputs of files as the input to another with | - \n");
	}
	else if ( strcmp(command[0], "cd") == 0 )
	{
		if (command[1] == '\0')
			{
				chdir(getenv("HOME"));
			}

		else
			chdir(command[1]);
	}
	else if ( strcmp(command[0], "pwd") == 0 )
	{
		{
			char buf[100];
			memset(buf, 0, sizeof(buf));
			printf("Current Directory: %s\n", getcwd(buf, sizeof(char) * 100));
		}
	}
	else
	{
		switch ((cpid = fork()) )
		{
		case -1:
			//failure to fork.
			perror("Failed to fork new process\n");
			exit(EXIT_FAILURE);

		case 0:
			//CHILD PROCESS
			if (input != 0)
			{
				dup2(input, 0);
			}
			if (output != 0)
			{
				dup2(output, 1);
			}
			if (pipes != 0 && pipes[0] != 0)
			{
				close(pipes[0]);
				close(pipes[1]);
			}
			execvp(command[0], command);
				//if it can get here, something went wrong.
			printf("Unknown command\n");

			exit(1);

		default:
			//case Parent
			signal(SIGINT, SigHandler);
			break;
		}

		if (cpid != 0)
		{
		//PARENT PROCESS
			if (pipes != 0 && pipes[1] != 0)
			{
				close(pipes[0]);
				close(pipes[1]);
			}
			for ( ; ; )
			{

				int status;
				cpid = wait(&status);
				if (cpid > 0) {
					if (WIFSIGNALED(status)) {
						fprintf(stderr, " child exited from signal %d\n", WTERMSIG(status));
					}
				}
				else {
					break;
				}
			}
		}


	}
	return 0;
}
